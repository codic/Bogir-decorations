# Bogir-decorations

## Overview

Bogir-decoration is a fork of the Breeze10 KDE window decoration meant for the Bogir theme.

## Credits

Breeze10: https://github.com/fauzie811/Breeze10 (see credits section for all relevant credits.)

## Dependencies

There are some dependencies you'll need to install to compile. Some people suggested using the following commands:

### Ubuntu or Debian based distros:
```bash
sudo apt install git g++ extra-cmake-modules cmake gettext libkf5config-dev libkdecorations2-dev libqt5x11extras5-dev qtdeclarative5-dev libkf5guiaddons-dev libkf5configwidgets-dev libkf5windowsystem-dev libkf5coreaddons-dev libfftw3-dev
```

### Arch based distros (including manjaro):
```bash
sudo pacman -S kdecoration qt5-declarative qt5-x11extras kcoreaddons kguiaddons kconfigwidgets kwindowsystem fftw cmake extra-cmake-modules
```

### OpenSUSE
``` bash
sudo zypper install git extra-cmake-modules libkdecoration2-devel kcoreaddons-devel kguiaddons-devel kconfig-devel kwindowsystem-devel ki18n-devel kconfigwidgets-devel libQt5DBus-devel libqt5-qtx11extras-devel fftw3-devel
```

## Installation

Once you have the dependencies listed above, to build manually, open a terminal inside the source directory and do:
```bash
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DKDE_INSTALL_LIBDIR=lib -DBUILD_TESTING=OFF -DKDE_INSTALL_USE_QT_SYS_PATHS=ON
make
sudo make install
```
Alternatively, run `./install.sh` for an automated build process.
After the intallation, restart KWin by logging out and in (or run `kwin_x11 --replace` from KRunner(open with Alt+Space), or `kwin_wayland --replace` if Wayland (untested!) is in use). Then, Bogir will appear in *System Settings &rarr; Application Style &rarr; Window Decorations*.


## Screenshots
![Konsole](screenshots/konsole.png)

![Dolphin](screenshots/dolphin.png)
